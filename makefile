#Compiler to use
CC=g++
#Compile flags - all targets are build with these
CFLAGS=-Wall -fexceptions -g -std=c++11
#Link flags
LDFLAGS=-pthread
#C+L flags for "release" build
OPTFLAGS=-O3 -flto

#List of all .cpp files to be compiled with full build.
SOURCES=src/main.cpp src/conf_parser.cpp src/udp_server.cpp src/routing_table.cpp
OBJECTS=$(SOURCES:.cpp=.o)

#Name of the finished executable
EXECUTABLE=router

#Targets and commands
all: $(SOURCES) $(EXECUTABLE)

#Builds up executable from object files, together with libraries
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $(EXECUTABLE)

#Every object file is dependent on its .cpp file
%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

#Build with release optimalization.
release: delete
	$(CC) -o $(EXECUTABLE)_rel $(CFLAGS) $(LDFLAGS) $(OPTFLAGS) $(SOURCES)

#Performs clean rebuild.
clean: delete
	make all

#Removes all object files from src/ subfolder (only those are left over from its build process.
delete:
	rm -f src/*.o	

