#include "routing_table.h"

// Default routing table constructor
routing_table::routing_table()
{
    ID = 0;
    RT = std::vector<std::pair<uchar, uchar> >(256);
    for (uchar i = 1; i > 0; i++)
    {
        RT[i].first = 0;
        RT[i].second = 0;
    }
    timers = std::vector<int>(256);
}

// ID setter
void routing_table::set_id(uchar i)
{
    ID = i;
}

// Add neighbour node
void routing_table::add_neighbour(const uchar &dest)
{
    RT[dest].first = dest; // no way specified
    RT[dest].second = 1; // one hop
    timers[dest] = TICKS; // reset TTL
}

// Generate RT message
std::string routing_table::export_routing_table(const uchar &destination)
{
    std::string temp;
    temp += (char)0; // routing message
    temp += (char)0; // verification
    temp += destination;
    temp += ID; // identify sender
    for (uchar i = 1; i > 0; i++)
    {
        if (i != destination && RT[i].first != destination && RT[i].second != 0) // send if the way exists (ie. hops > 0)
        {
            temp += i;
            temp += RT[i].second; // hops
        }
    }
    temp += (char)0; // ending symbol
    return temp;
}

// Process RT message and import new paths if they are better
void routing_table::import_routing_table(const std::string &message)
{
    // Check if message is valid
    if (message[0] != 0 || message[1] != 0)
    {
        return;
    }
    // Get sender
    uchar source = message[3];
    int index = 4;
    // Decrease timers
    for (uchar i = 1; i > 0; i++)
    {
        if (timers[i] == 1) // Remove records if they expire
        {
            remove_node(i);
        }
        timers[i]--;
    }

    RT[source].first = source; // no way specified
    RT[source].second = 1; // one hop
    timers[source] = TICKS;
    // Process all routes until termination signal
    while (message[index] != 0)
    {
        if (RT[message[index]].second == 0 || RT[message[index]].second >= message[index+1] + 1)
        {
            RT[message[index]].first = source;
            RT[message[index]].second = message[index+1] + 1;
            timers[message[index]] = TICKS;
        }
        index += 2;
    }
}

// Gets the right neighbour ID (or 0 if no known path exists)
uchar routing_table::get_way(const uchar &destination)
{
    return RT[destination].first;
}

// Remove node from RT
void routing_table::remove_node(const uchar &destination)
{
    RT[destination].first = 0;
    RT[destination].second = 0;
    // If neighbour, remove all nodes connected through it
    for (uchar i = 1; i > 0; i++)
    {
        if (RT[i].first == destination)
        {
            RT[i].first = 0;
            RT[i].second = 0;
        }
    }
}

// Default routing table destructor
routing_table::~routing_table()
{
}
