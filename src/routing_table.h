#ifndef ROUTING_TABLE_H
#define ROUTING_TABLE_H

#include "structs.h"
#include <vector>
#include <string>

class routing_table
{
    public:
        routing_table();
        void set_id(uchar i);
        std::string export_routing_table(const uchar &destination);
        void import_routing_table(const std::string &message);
        uchar get_way(const uchar &destination);
        void remove_node(const uchar &destination);
        void add_neighbour(const uchar &dest);
        virtual ~routing_table();
    protected:
    private:
        uchar ID;
        std::vector<std::pair<uchar, uchar> > RT; // <way, hops>
        std::vector<int> timers; // TTLs
        static const int TICKS = 16; // Time To Live (TTL) - 16 import cycles
};

#endif // ROUTING_TABLE_H
