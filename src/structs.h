#ifndef STRUCTS
#define STRUCTS

typedef unsigned char uchar;
#include <string>

/* struct representing IP address, contains ip as a human readable string, port as an int and ID as unsigned char.
*/
struct ip_address {
    ip_address(std::string i, int p, uchar id){
        ip = i;
        port = p;
        ID = id;
    }
    ip_address(){
        ip = "";
        port = 0;
        ID = 0;
    }
    std::string ip;
    int port;
    uchar ID;
};

#endif //structs
