#ifndef CONF_PARSER_H
#define CONF_PARSER_H

#include <vector>
#include <string>

#include "structs.h"
/* Class made for udp_server to parse specified .cfg file format.
    Has no dynamically allocated memory, so destructor is left to the compiler and scoping.

    cfg file format is
    int inside 1-255 range. (representable as uchar)
    port on which this instance is supposed to listen on
    int number of connections specified below
    connections in format <string, ip address> <int, port> <ID of target>

    takes path to config file in the constructor/
*/
class conf_parser{

    public:
        conf_parser(const std::string& filename);
        bool read_config();
        int get_port();
        int get_ID();
        std::vector<std::string>& get_err();
        std::vector<ip_address> get_clients();
    protected:
    private:
        std::vector<std::string> err;
        std::string path_to_file;
        std::vector<ip_address> addresses;
        int ID, port;
        bool success;

        bool validate_ip_address(const std::string& address);
        ip_address parse_connection(const std::string& address);

};

#endif // CONF_PARSER_H
