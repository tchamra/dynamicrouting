#include "udp_server.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include <netdb.h>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <algorithm>
#include <sstream>

//copied hack, renamed to avoid namespace pronblems.
int ssstr_atoi(const std::string &str){
    std::stringstream conv(str);
    int val;
    conv >> val;
    return val;
}


#define SOCKET_ERR 3

//The only constructor that should be usedm beware default construtor.
udp_server::udp_server(int p, uchar i, std::vector<ip_address> c){
    ID = i;
    port = p;
    connections = c;
    rt.set_id(i);
    for (size_t i = 0; i < c.size(); i++){
        rt.add_neighbour(c[i].ID);
    }
}

//Message starting with 0 as address is (supposedly) RT update.
//There are more validity checks in RT itself.
inline bool udp_server::is_rt_update(const std::string& pay){
    return pay[0] == 0;
}

//Returns true if calling thread's string was filled with message, false otherwise.
//Temporarily locks msg queue mutex.
bool udp_server::get_next_message(std::string& r){
    pthread_mutex_lock(&q_mutex);
    if (request_queue.empty()){
        pthread_mutex_unlock(&q_mutex);
        return false;
    } else {
        std::cerr << "Getting next msg: " << request_queue.front() << std::endl;
        r.assign(request_queue.front());
        request_queue.pop();
        pthread_mutex_unlock(&q_mutex);
        return true;
    }
}

//Locks msg queue mutex and appends given string to the queue.
//Doesn't use move constructor because of c++11 compatibility problems. (and some constness trouble)
void udp_server::add_message(const std::string& r){
    pthread_mutex_lock(&q_mutex);
    request_queue.push(r);
    pthread_mutex_unlock(&q_mutex);
}

//Starts up the whole server. Holds the entrant thread and spawns 3 more for work.
//Then continues to read from std.in in permanent loop.
void udp_server::start(){

    //Detaches threads... theoretically, even if this thread dies, the others could keep the server going as rebroadcaster.
    pthread_t listener, worker, broadcaster;
    pthread_create(&listener, 0, listen_wrapper, this);
    pthread_detach(listener);
    pthread_create(&worker, 0, work_wrapper, this);
    pthread_detach(worker);
    pthread_create(&broadcaster, 0, rt_update_wrapper, this);
    pthread_detach(broadcaster);

    while (1){
    //Permanent (blocking) loop over std.in
        std::cerr << "Main thread is listening to std.in" << std::endl;
        int target;
        char buff[129];
        memset(buff, 0, sizeof(buff));
        /* Messages from std.in are limited to 128 bytes.
           Note, this doesn't apply for message that could be theoretically sent in from the network
           Those are limited to 600 chars (potential size of RT table + some extra).
        */
        if (scanf("%d:%128s", &target, buff) == 2){
            if (target < 0 || target > 255){
                std::cerr << "Target is not in valid range, request is discarded." << std::endl;
            } else {
                //Constructs message from given data and adds it to msg queue.
                char dest = (char) target;
                std::string temp;
                temp += dest;
                temp += ":";
                std::string t2(buff);
                temp += t2;
                add_message(temp);
            }
        }
    }
}

//For request to be a message, the first byte has to be non-zero (those are RT updates)
//And the second byte has to be ":" (very basic garbage check, chosen for simplicity sake
inline bool udp_server::is_message(const std::string& r){
    return r[0] != 0 && r[1] == ':';
}

//If the target of given request is equal to server's ID, the message is "local"
//--- It was supposed to terminate here.
inline bool udp_server::is_local(const std::string& r){
    return r[0] == ID;
}

//Permanent loop periodically rebroadcasting server's version of RT table.
//While broadcasting locks rt mutex.
//Period of broadcast is defined in udp_server header file as time_to_broadcast.
void udp_server::rt_update(){
    while(true){
        sleep(time_to_broadcast);
        std::cerr << "Broadcasting!" << std::endl;
        pthread_mutex_lock(&rt_mutex);
        for (size_t i = 0; i < connections.size(); i++){
            std::string rt_message = rt.export_routing_table(connections[i].ID);
            send_message(rt_message);
        }
        pthread_mutex_unlock(&rt_mutex);
        }
}

//Hosts the main work-thread of the server.
//Is a permanent loop that works until the request queue is empty, then sleeps for a time
//This time is defined as time_to_sleep in udp_server header file.
//Determines whether request is valid request, then if it is RT update, message and so on.
void udp_server::work(){
    std::cerr << "Working!" << std::endl;
    std::string current;
    while (true){
        if (get_next_message(current)){
            //There was a request to be processed in the queue.
            std::cerr << "message: " << current << std::endl;
            if (is_rt_update(current)){
                //If the message looks like it is routing table update, send it to RT.
                std::cerr << "RT updated with: " << current << std::endl;
                pthread_mutex_lock(&rt_mutex);
                rt.import_routing_table(current);
                pthread_mutex_unlock(&rt_mutex);
            } else {
                //Logic to work with the messages.
                if (is_message(current)){
                    std::cerr << "Message: " << current << " was determined to be a valid payload." << std::endl;
                    if (is_local(current)){
                        std::cerr << "Message was determined to be local." << std::endl;
                        std::cout << current.substr(current.find(':')+1) << std::endl;
                    } else {
                        // pass the message on.
                        send_message(current);
                    }
                }
            }
        } else {
            //The request queue is empty, take a break.
            sleep(time_to_sleep);
        }
    }
}

//Sends the message to correct target.
//Has minimal mutexing, so there can be more threads inside at once.
void udp_server::send_message(const std::string& r){
    if (r.size() < 3){
        std::cerr << "Invalid request - request is too short for use." << std::endl;
    }
    char target = r[0];
    ip_address ip;
    if (target == 0 && r[1] == 0){
        std::cerr << "Sending rt table" << std::endl;
        //The target of rt update is the third byte, instead of first.
        ip = get_ip_address(r[2]);
    } else {
        std::cerr << "Sending message" << std::endl;
        ip = get_ip_address(target);
    }
    if (ip.port == 0){
        //No path to our target was found, could check for any member of ip.
        //Drops the message. (Perhaps reimplement to give it a retry?)
        std::cerr << "No connection for routing found!" << std::endl;
    } else {
        sockaddr_in target;
        int s, slen = (sizeof(target));
        char buffer[buffer_size];
        memset(buffer, 0, sizeof(buffer));
        if ((s=socket(AF_INET, SOCK_DGRAM, 0))==-1){
            //Error while opening socket for sending.
            //Returns message to queue.
            std::cerr << "Couldnt open socket for sending, returning message to queue." << std::endl;
            add_message(r);
        } else {
            //Sockets was opened succesfully and we can now send the message.
            memset((char* ) &target, 0, sizeof(target));
            target.sin_family = AF_INET;
            target.sin_port = htons(ip.port);
            if (inet_aton(ip.ip.c_str(), &target.sin_addr) == 0){
                //This shouldn't happen, indicates error in parser or routing table.
                std::cerr << "Couldn't resolve ip address format. Message has been dropped." << std::endl;
            } else {
                std::cerr << "Sending msg: " << r << std::endl;
                if (sendto(s, r.c_str(), r.size(), 0, (struct sockaddr *)&target, slen) == -1){
                    //Message couldn't be sent due to local problems.
                    std::cerr << "Problems while sending the message, message return to queue." << std::endl;
                    add_message(r);
                } else {
                    std::cerr << "Message sent." << std::endl;
                }
            }
        }
    }

}

//Returns sending target from the local copy of routing table.
//First attempts to find the target in one hop - that is to go through the servers open connections.
ip_address udp_server::get_ip_address(uchar target){
    for (size_t i = 0; i < connections.size(); i++){
        if (connections[i].ID == target){
            return connections[i];
        }
    }
    //Couldn't find one hop route, lock rt_mutex so it isn't overwritten underneath and ask it.
    pthread_mutex_lock(&rt_mutex);
    uchar temp = rt.get_way(target);
    pthread_mutex_unlock(&rt_mutex);
    for (size_t i = 0; i < connections.size(); i++){
        if (connections[i].ID == temp){
            return connections[i];
        }
    }
    return ip_address();
}

//Endless loop listening to udp packets.
//Whenever there is UDP packet sent to server's port, appends it to request queue and listens again.
//Performs no request checking, that is responsibility of the worker thread.
void udp_server::listen(){
    std::cerr << "Listening!" << std::endl;

    //Create socket.
    struct sockaddr_in me, other;
    int s;
    socklen_t slen = sizeof(other);
    s = socket(AF_INET, SOCK_DGRAM, 0);
    if (s == -1){
        std::cerr << "Failed to open socket." << std::endl;
        _exit(SOCKET_ERR);
    }

    //Bind socket.
    memset((char *)&me, 0, sizeof(me));
    me.sin_family = AF_INET;
    me.sin_port = htons(port);
    me.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(s, (struct sockaddr *)&me, sizeof(me)) == -1){
        std::cerr << "Failed to bind socket." << std::endl;
        _exit(SOCKET_ERR);
    }

    char buffer[buffer_size];
    while (true){
        //Socket was successfuly created and bound to requested port, now we can receive requests.
        memset(buffer, 0, sizeof(buffer));
        std::cerr << "Attempting to receive msg." << std::endl;
        int message_len = recvfrom(s, buffer, buffer_size, 0,(struct sockaddr *) &other, &slen);
        std::string str(buffer, message_len+1);
        add_message(str);
    }
}

//Short wrappers letting threads to re-enter the class.
void* listen_wrapper(void* arg){
    udp_server* srv = (udp_server*) arg;
    srv->listen();
    pthread_exit(0);
}

void* work_wrapper(void* arg){
    udp_server* srv = (udp_server*) arg;
    srv->work();
    pthread_exit(0);
}

void* rt_update_wrapper(void* arg){
    udp_server* srv = (udp_server*) arg;
    srv->rt_update();
    pthread_exit(0);
}
