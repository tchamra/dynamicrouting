#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>

#include "conf_parser.h"
#include "udp_server.h"
#include "routing_table.h"
#include "structs.h"

#define conf_parser_error 2

// Write help text
void help_and_exit(char *self) {
	std::cerr << "Dynamic Routing (Task 4)" << std::endl;
	std::cerr << "Usage: " << self << " [--help] [config file name]" << std::endl;
	std::cerr << "Error codes: 3 for server errors, 2 for config file related errors, 1 for generic errors." << std::endl;
	exit(0);
}

// Function used to report config parser error logs
void print_conf_errors(const std::vector<std::string>& err_log){
    for (size_t i = 0; i < err_log.size(); i++){
        std::cerr << err_log[i] << std::endl;
    }
    exit(conf_parser_error);
}

// Function used to report errors
void error(const std::string& msg, int err_no){
    std::cerr << msg << std::endl;
    exit(err_no);
}

// Program entry point
int main(int argc, char *argv[])
{
	// Config variables
	uchar ID;
	int port, connectionsCount;
	std::vector<ip_address> connections;

	// Terminate if no config is given
	if (argc < 2 || strcmp("-h", argv[1]) == 0 || strcmp("--help", argv[1]) == 0)
	{
		std::cerr << "No configuration file specified" << std::endl;
		help_and_exit(argv[0]);
	}

	// Load config
	// Conf_parser is scoped, so it gets destroyed after it finishes.
	{conf_parser config(argv[1]);
	if (config.read_config()){
        ID = config.get_ID();
        port = config.get_port();
        connections = config.get_clients();
        connectionsCount = connections.size();
	} else {
        print_conf_errors(config.get_err());
	}}

    // Print configuration details
    std::cerr << "My ID is " << (int)ID << " on port " << port << ". I can accept " << connectionsCount << " connections as server." << std::endl;
	for (uchar i = 0; i < connectionsCount; i++){
		std::cerr << "Client IP " << connections[i].ip << " on port " << connections[i].port << std::endl;
	}

	// Start server
	udp_server server(port, ID, connections);
	server.start();

	return 0;
}
