#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include "routing_table.h"
#include "structs.h"
#include <string>
#include <vector>
#include <queue>
#include <pthread.h>

#define buffer_size 600
#define time_to_broadcast 5
#define time_to_sleep 1

typedef unsigned char uchar;

/* Class representing udp server, listening at given (p)ort and reporting with given (i)d, knows about given (c) neighbours.

    Public methods are all blocking and endless loops - entry points should always be start.
    start() spawns 3 new threads, one for listening, one for processing datagrams and one for signaling routing table updates.

    Dependent on rt_table class to provide it with indirect routing info. Direct routing is possible even with faulty implementation.


    listens at std.in for more messages in format of <ID>:<message, up to 128 chars>
*/


class udp_server{
    public:
        udp_server(int p, uchar i, std::vector<ip_address> c);
        void start();
        void listen();
        void work();
        void rt_update();
    protected:
    private:
        bool is_rt_update(const std::string& pay);
        void add_message(const std::string& r);
        bool get_next_message(std::string& r);
        bool is_message(const std::string& r);
        bool is_local(const std::string& r);
        void send_message(const std::string& r);
        void handle_rt_update(const std::string& r);
        ip_address get_ip_address(uchar target);

        routing_table rt;
        int port;
        uchar ID;
        std::vector<ip_address> connections;

        std::queue<std::string> request_queue;
        pthread_mutex_t q_mutex = PTHREAD_MUTEX_INITIALIZER;
        pthread_mutex_t rt_mutex = PTHREAD_MUTEX_INITIALIZER;

};
//Function prototypes for wrappers for pthreads entry points.
void* work_wrapper(void* arg);
void* listen_wrapper(void* arg);
void* rt_update_wrapper(void* arg);

#endif // UDP_SERVER_H
