#include "conf_parser.h"
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cctype>


//helper method, because of problems with c++11 stl (std::stoi)
int sstr_atoi(const std::string &str){
    std::stringstream conv(str);
    int val;
    conv >> val;
    return val;
}

//Different hack around, pick later
/*
#include <cstdlib>
int sstr_atoi(const std::string &str){
    return atoi(str.c_str());
}
*/

//This constructor is required for parser to function. Beware of default constructors.
conf_parser::conf_parser(const std::string& filename){
    path_to_file = filename;
}

//Main parsing function. Returns true if reading has succeeded, false if it failed at any point.
//Finishes parsing the conf file in all cases, tries to enumerate all the errors found.
bool conf_parser::read_config(){
    success = true;
    std::string line;
    std::ifstream file(path_to_file.c_str());
    if (file.is_open()){
            //extract ID of this server
            std::getline(file, line);
            ID = sstr_atoi(line);
            if (ID < 1 || ID > 255){
                success = false;
                err.push_back("The ID in conf file is not valid. (Valid ID is number inside 1-255.)");
            }
            //extract port number
            std::getline(file, line);
            port = sstr_atoi(line);
            if (port < 1 || port > 65535){
                success = false;
                err.push_back("The port in conf file is not valid. (Valid port is number inside range 1-65535.)");
            }
            std::getline(file, line);
            int connections = sstr_atoi(line);
            if (connections < 1 || connections > 4){
                success = false;
                err.push_back("The number of connections in conf file is not valid. (Valid number of connections is inside range 1-4.)");
            }
            for (int i = 0; i < connections; i++){
                std::getline(file, line);
                //Skips empty lines while reading in ip addresses and ports, if there are any in the conf file.
                while(line == ""){
                    std::getline(file, line);
                }
                //Attempts to parse the line as connection info.
                addresses.push_back(parse_connection(line));
            }
    } else {
        err.push_back("Failed to open file.");
        success = false;
    }

    file.close();
    return success;

}

//Parses IP address from connection lines.
//Reports when the line is malformed, only finds and reports the first problem for given line.
ip_address conf_parser::parse_connection(const std::string& connection){
    int address_length, port_start, address_start;
    std::string::const_iterator it, start;
    it = connection.begin();
    //Skip over any white space at the start of the line.
    while(isspace(*it)){
        it++;
    }

    //Find the ip address.
    int dot_count = 0;
    start = it;
    std::string ip;
    for (; it < connection.end(); it++){
        if (isspace(*it) && dot_count != 3){
            success = false;
            err.push_back("The following line in config file appears to be malformed: " + connection);
            break;
        } else if ((*it) == '.'){
            dot_count++;
        } else if (dot_count == 3 && isspace(*it)){
            address_length = std::distance(start, it);
            address_start = std::distance(connection.begin(), start);
            if (!validate_ip_address(connection.substr(address_start, address_length))){
                success = false;
                err.push_back("The IP address in this line seems to be malformed: " + connection);
            } else {
                ip = connection.substr(address_start, address_length);
            }
            break;
        }
    }

    //skip possible remaining whitespace
    while (isspace(*it)){
            it++;
    }
    port_start = std::distance(connection.begin(), it);

    //find end of the port no
    while (!(isspace(*it))){
        it++;
    }
    int port_end = std::distance(connection.begin(), it) - port_start;
    int port_no = sstr_atoi(connection.substr(port_start, port_end));
    if (port_no > 65535 || port_no < 1){
        success = false;
        err.push_back("The port number in this line seems to be outside of valid range: " + connection);
    }

    while(isspace(*it)){
        it++;
    }

    int temp_id = sstr_atoi(connection.substr(std::distance(connection.begin(), it)));
    if (temp_id < 1 || temp_id > 255){
        success = false;
        err.push_back("The id of this connection seems to be outside of valid range: " + connection);
    }

    uchar id = temp_id;

    std::cerr << "Parsing: " << connection << std::endl;
    std::cerr << "Parsed: ip: " << ip << "  port: " << port_no << "  id: " << (int)id << std::endl;
    return ip_address(ip, port_no, id);
}

//Provides public access to parsed ID.
int conf_parser::get_ID(){
    return ID;
}

//Provides public access to parsed port.
int conf_parser::get_port(){
    return port;
}

//Provides public access to parsed IP addresses.
std::vector<ip_address> conf_parser::get_clients(){
    return addresses;
}

//Provides access to errors during the parsing.
std::vector<std::string>& conf_parser::get_err(){
    return err;
}

//Checks if the string given contains valid IP address formated as [0-255].[0-255].[0-255].[0-255]
bool conf_parser::validate_ip_address(const std::string& address){
    size_t last_index = 0, new_index = 0;
    for (size_t i = 0; i < address.size(); i++){
        if (!isdigit(address[i]) && address[i] != '.'){
            //Only allowed nondigit character is '.', otherwise it is invalid.
            return false;
        }
        //Find breakpoint for the IP address then validate it by breakpoints.
        if (address[i] == '.'){
            last_index = new_index;
            new_index = i;
            //ip_address validation goes here.
            int temp = sstr_atoi(address.substr(last_index, new_index-last_index+1));
            if (temp < 0 || temp > 255){
                return false;
            }
        }
    }
    return true;
}
